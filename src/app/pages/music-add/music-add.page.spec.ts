import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MusicAddPage } from './music-add.page';

describe('MusicAddPage', () => {
  let component: MusicAddPage;
  let fixture: ComponentFixture<MusicAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MusicAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
