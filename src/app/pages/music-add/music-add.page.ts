import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MusicService} from '../../shared/services/music.service';
import {ModalController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-music-add',
  templateUrl: './music-add.page.html',
  styleUrls: ['./music-add.page.scss'],
})
export class MusicAddPage implements OnInit {
  form: FormGroup;

  constructor(private fBuilder: FormBuilder, private musicService: MusicService, private navController: NavController, private modalController: ModalController) { }

  ngOnInit() {
    this.form = this.fBuilder.group({
      title: new FormControl('', [Validators.required]),
      artist: new FormControl('', [Validators.required])
    });
  }

  onSubmitAction() {
    console.log(this.form.value);
    if (this.form.valid) {
      this.musicService.addMusic(this.form.value).subscribe(data => console.log(data));
      this.navController.back();
    }
  }

  onBackAction() {
    this.navController.back();
  }
}
