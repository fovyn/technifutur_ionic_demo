import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MusicAddPageRoutingModule } from './music-add-routing.module';

import { MusicAddPage } from './music-add.page';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MusicAddPageRoutingModule,
    SharedModule,
      ReactiveFormsModule
  ],
  declarations: [MusicAddPage]
})
export class MusicAddPageModule {}
