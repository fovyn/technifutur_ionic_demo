import { Component, OnInit } from '@angular/core';
import {MusicService} from '../../shared/services/music.service';
import {Observable} from 'rxjs';
import {Music} from '../../shared/models/music';

@Component({
  selector: 'app-music-list',
  templateUrl: './music-list.page.html',
  styleUrls: ['./music-list.page.scss'],
})
export class MusicListPage implements OnInit {
  musics: Music[] = [];

  constructor(private musicService: MusicService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.musicService.getMusics().subscribe(data => this.musics = data);
  }


  get rows(): Array<Music[]> {
    const aRows = [];

    const nbRows = (this.musics.length % 4);

    for (let i = 0; i < nbRows; i++) {
      const row = [];
      for (let j = 0; j < 4; j++) {
        if (this.musics[i + j]) {
          row.push(this.musics[i + j]);
        }
      }
      aRows.push(row);
    }
    return aRows;
  }

  onEditAction(music: Music) {
    console.log(music);
  }
}
