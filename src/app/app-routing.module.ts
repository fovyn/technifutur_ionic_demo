import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'music-list',
    loadChildren: () => import('./pages/music-list/music-list.module').then( m => m.MusicListPageModule)
  },
  {
    path: '',
    redirectTo: 'music-list',
    pathMatch: 'full'
  },
  {
    path: 'music-add',
    loadChildren: () => import('./pages/music-add/music-add.module').then( m => m.MusicAddPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
