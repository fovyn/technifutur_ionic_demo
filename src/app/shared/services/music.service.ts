import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Music} from '../models/music';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private httpClient: HttpClient) { }

  addMusic(music: Music): Observable<any> {
    return this.httpClient.post<Music>('/api/musics', music).pipe(
        tap(_ => console.log('Music Added')),
        catchError(this.handleError<Music[]>('Add Music', []))
    );
  }

  getMusic(id: number): Observable<Music> {
    return this.httpClient.get<Music>(`/api/musics/${id}`).pipe(
        tap(_ => console.log('Music Get One')),
        catchError(this.handleError<Music>('Get One Music', null))
    );
  }

  getMusics(): Observable<Music[]> {
    return this.httpClient.get<Music[]>(`/api/musics`).pipe(
        tap(_ => console.log('Music Get All')),
        catchError(this.handleError<Music[]>('Get All Music', []))
    );
  }

  deleteMusic(id): Observable<any> {
    return this.httpClient.delete(`/api/musics/${id}`).pipe(
        tap(_ => console.log('Music Get One')),
        catchError(this.handleError<Music>('Get One Music', null))
    );
  }

  private handleError<T>(operation= 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
