export interface Music {
    title: string;
    artist: string;
}
